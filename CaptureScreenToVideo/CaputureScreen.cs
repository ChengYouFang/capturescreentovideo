﻿using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Profiles;
using Microsoft.Expression.Encoder.ScreenCapture;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace CaptureScreenToVideo
{
    public partial class CaputureScreen : Form
    {
        /// <summary>
        /// 擷取螢幕程序
        /// </summary>
        private ScreenCaptureJob _screenJob = new ScreenCaptureJob();

        /// <summary>
        /// 音訊裝置
        /// </summary>
        private Collection<EncoderDevice> audioDevices;

        /// <summary>
        /// 主視窗尺寸
        /// </summary>
        private Rectangle _size;

        /// <summary>
        /// 畫面品質
        /// </summary>
        private int _quality  { get { return trackBarQuality.Value; } }

        /// <summary>
        /// 檔案路徑
        /// </summary>
        private String _path = String.Empty;

        /// <summary>
        /// 檔案名稱
        /// </summary>
        private String _fileName = String.Empty;
        
        /// <summary>
        /// 執行緒
        /// </summary>
        private Thread _thread;

        /// <summary>
        /// Lock機制
        /// </summary>
        private bool _isLock =false;

        public CaputureScreen()
        {
            InitializeComponent();
        }

        private void CaputureScreen_Load(object sender, EventArgs e)
        {
            comboBoxAudioDevice.SelectedIndex = 0;

            //尋找音訊裝置
            audioDevices = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            foreach (var audio in audioDevices)
                comboBoxAudioDevice.Items.Add(audio.Name);
        }

        private void CaputureScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((_thread != null && _thread.IsAlive) || _isLock || this.buttonStart.Text == "停止錄影")
            {
                if (MessageBox.Show("目前仍在錄影或轉檔，確定要關閉嗎？",
                    "目前還在執行中", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    _screenJob.Stop();
                    _screenJob.Dispose();
                }
                else
                    e.Cancel = true;
            }
        }


        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (_isLock == false)
            {
                if (this.buttonStart.Text == "開始錄影")
                    StartRecordVideo();
                else
                {
                    this.buttonStart.Text = "開始錄影";
                    StopRecordScreenVideo();
                }
            }
            else
                MessageBox.Show("請等待轉檔完成後再開始錄影");
        }


        private void Job_EncodeCompleted(object sender, EncodeCompletedEventArgs e)
        {
            MessageBox.Show("處理完畢");
            _isLock = false;
            if (File.Exists(String.Format(@"{0}\{1}.xesc", _path, _fileName)))
                File.Delete(String.Format(@"{0}\{1}.xesc", _path, _fileName));
        }

        private void trackBarQuality_ValueChanged(object sender, EventArgs e)
        {
            if (this.trackBarQuality.Value >= 80)
                this.label3.Text = "好";
            else if (this.trackBarQuality.Value >= 45)
                this.label3.Text = "普通";
            else
                this.label3.Text = "差";
        }


        private void StartRecordVideo()
        {
            //輸出資料夾
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.buttonStart.Text = "停止錄影";
                
                //檔案路徑
                _path = dialog.SelectedPath;
                
                //檔案名稱
                _fileName = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");

                if (comboBoxAudioDevice.SelectedIndex > 0)
                    _screenJob.AddAudioDeviceSource(audioDevices.ElementAt(comboBoxAudioDevice.SelectedIndex-1));

                //輸出檔案路徑
                _screenJob.OutputScreenCaptureFileName = String.Format(@"{0}\{1}.xesc", _path, _fileName);
                
                //主螢幕尺寸
                _size = Screen.PrimaryScreen.Bounds;

                //擷取範圍
                _screenJob.CaptureRectangle = new Rectangle(0, 0, _size.Width, _size.Height);

                //畫質
                _screenJob.ScreenCaptureVideoProfile.Quality = _quality;

                //張數
                _screenJob.ScreenCaptureVideoProfile.FrameRate = 30;

                //擷取滑鼠
                _screenJob.CaptureMouseCursor = true;

                //開始擷取
                _screenJob.Start();
            }
        }

        /// <summary>
        /// 停止錄影
        /// </summary>
        private void StopRecordScreenVideo()
        {
            _screenJob.Stop();
            if (_thread != null)
                _thread.Abort();
            _thread = new Thread(new ThreadStart(Run));
            _thread.Priority = ThreadPriority.Highest;
            _thread.Start();
        }


        /// <summary>
        /// 執行緒方法
        /// </summary>
        private void Run()
        {
            MediaItem media = new MediaItem(String.Format(@"{0}\{1}.xesc", _path, _fileName));

            //輸出格式
            media.OutputFormat = new WindowsMediaOutputFormat();

            //輸出影像格式
            media.OutputFormat.VideoProfile = new SimpleVC1VideoProfile();

            //Bit Rate
            media.OutputFormat.VideoProfile.Bitrate = new VariableConstrainedBitrate(192, 192);

            //輸出影像尺寸
            media.OutputFormat.VideoProfile.Size = _size.Size;

            //輸出張數
            media.OutputFormat.VideoProfile.FrameRate = 30;

            //輸出音源格式
            media.OutputFormat.AudioProfile = new WmaAudioProfile();

            //Bit Rate
            media.OutputFormat.AudioProfile.Bitrate = new VariableConstrainedBitrate(128, 192);

            //輸出編碼
            media.OutputFormat.AudioProfile.Codec = AudioCodec.Wma;

            //Bits
            media.OutputFormat.AudioProfile.BitsPerSample = 24;

            Job job = new Job();

            //加入轉檔行程
            job.MediaItems.Add(media);

            //預設輸出路徑
            job.OutputDirectory = String.Format(@"{0}\", _path);

            //不建立子資料夾
            job.CreateSubfolder = false;
            
            //預設輸出檔案名稱
            job.DefaultMediaOutputFileName = String.Format(@"{0}.avi", _fileName);
            
            //轉換格式
            job.ApplyPreset(Presets.VC1ScreenEncodingVBR);
            
            //完成後會觸發該事件
            job.EncodeCompleted += Job_EncodeCompleted;

            _isLock = true;

            //開始編碼
            job.Encode();
        }


    }
}
